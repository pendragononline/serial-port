﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Management;
using System.Diagnostics.Tracing;
using System.Windows.Forms;

namespace serial_port
{
    class SerialPortProgram
    {
        private SerialPort port = new SerialPort("COM1", 9600, Parity.Even, 7, StopBits.One);

        [STAThread]
        static void Main(string[] args)
        {
            // Instatiate this class
            new SerialPortProgram();
        }

        private SerialPortProgram()
        {
            Console.WriteLine("Incoming Data:");
            port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);

            try
            {
                port.Open();
                byte[] weightCmd = Encoding.UTF8.GetBytes("W\r\n");
                port.Write(weightCmd, 0, weightCmd.Length);
                Application.Run(); // Enter an application loop to keep this thread alive
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot connect to serial port! Error: " + e);
                Console.ReadLine();
            }
        }

        private void port_DataReceived(object sender,
          SerialDataReceivedEventArgs e)
        {
            Console.WriteLine(port.ReadExisting());
        }
    }

}
    /*class Program
    {
        static void Main(string[] args)
        {
            getPortInfo();
            getPortData();
            programExit();
        }

        private static void getPortInfo() //GET INFO ABOUT SERIAL PORTS
        {
            try
            {
                using (var searcher = new ManagementObjectSearcher
                ("SELECT * FROM WIN32_SerialPort"))
                {
                    string[] portnames = SerialPort.GetPortNames();
                    var ports = searcher.Get().Cast<ManagementBaseObject>().ToList();
                    var tList = (from n in portnames
                                 join p in ports on n equals p["DeviceID"].ToString()
                                 select n + " - " + p["Caption"]).ToList();

                    tList.ForEach(Console.WriteLine);
                }
            }
            catch
            {
                Console.WriteLine("No serial ports detected.");
            }
        }

        private static void getPortData() //GET DATA FROM SERIAL PORT
        {
            try
            {
                SerialPort scalesPort = new SerialPort("COM1");

                scalesPort.BaudRate = 9600;
                scalesPort.Parity = Parity.Even;
                scalesPort.StopBits = StopBits.One;
                scalesPort.DataBits = 7;
                scalesPort.Handshake = Handshake.None;

                scalesPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

                scalesPort.Open();
                scalesPort.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot connect to serial port! Error: " + e);
                Console.ReadLine();
            }
        }

        private static void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e) //RECEIVE DATA FROM SCALES
        {
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadExisting();
            if (indata == null)
            {
                Console.WriteLine("No data received.");
            }
            else
            {
                Console.WriteLine("Data Received:");
                Console.WriteLine(indata);
            }
        }

        private static void programExit()
        {
            Console.WriteLine("End of program. Press enter to exit");
            Console.ReadLine();
        }
    }
}*/


